package nl.rug.digitallab.gradle.plugin.java

import nl.rug.digitallab.gradle.plugin.common.CommonJavaPlugin
import nl.rug.digitallab.gradle.plugin.common.git.GitPlugin
import nl.rug.digitallab.gradle.util.VersionPropertiesUtils.readVersionProperty
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.testing.jacoco.plugins.JacocoPlugin

/**
 * The JavaBasePlugin is a plugin that is used for shared configuration between [JavaProjectPlugin]
 * and [JavaLibraryPlugin].
 */
class JavaBasePlugin: Plugin<Project> {
    override fun apply(project: Project) {
        val junitVersion = project.readVersionProperty("junitVersion")

        with(project.pluginManager) {
            apply(CommonJavaPlugin::class.java) // Inherit the base java plugin

            apply(JavaPlugin::class.java) // Add the java plugin to the project.
            apply(GitPlugin::class.java) // Add the Git plugin for releases
        }

        with(project.dependencies) {
            add("testImplementation", platform("org.junit:junit-bom:${junitVersion}"))
            add("testImplementation", "org.junit.jupiter:junit-jupiter")
        }
    }
}
