package nl.rug.digitallab.gradle.plugin.java.project

import nl.rug.digitallab.gradle.plugin.java.JavaBasePlugin
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * The JavaProjectPlugin is a plugin that is used to configure a Java project.
 */
class JavaProjectPlugin: Plugin<Project> {
    override fun apply(project: Project) = with(project.pluginManager) {
        // Apply the non-framework specific configuration
        apply(JavaBasePlugin::class.java)
    }
}
