package nl.rug.digitallab.gradle.plugin.java.library

import nl.rug.digitallab.gradle.plugin.common.publish.PublishablePlugin
import nl.rug.digitallab.gradle.plugin.java.JavaBasePlugin
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * The JavaLibraryPlugin is a plugin that is used to configure a Java library project.
 */
class JavaLibraryPlugin: Plugin<Project> {
    override fun apply(project: Project) = with(project.pluginManager) {
        // Apply the non-framework specific configuration
        apply(JavaBasePlugin::class.java)
        apply(PublishablePlugin::class.java)
    }
}
