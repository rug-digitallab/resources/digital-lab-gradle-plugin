package nl.rug.digitallab.gradle.plugin.kotlin.library

import nl.rug.digitallab.gradle.plugin.common.publish.PublishablePlugin
import nl.rug.digitallab.gradle.plugin.kotlin.KotlinBasePlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.kordamp.gradle.plugin.jandex.JandexPlugin

/**
 * The KotlinLibraryPlugin is a plugin that is used to configure a Kotlin library project.
 */
class KotlinLibraryPlugin: Plugin<Project> {
    override fun apply(project: Project) = with(project.pluginManager) {
        // Apply the non-framework specific configuration
        apply(JandexPlugin::class.java)
        apply(KotlinBasePlugin::class.java)
        apply(PublishablePlugin::class.java)
    }
}
