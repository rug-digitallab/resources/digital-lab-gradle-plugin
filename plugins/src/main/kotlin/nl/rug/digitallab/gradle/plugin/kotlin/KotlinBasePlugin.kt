package nl.rug.digitallab.gradle.plugin.kotlin

import nl.rug.digitallab.gradle.plugin.common.CommonKotlinPlugin
import nl.rug.digitallab.gradle.util.VersionPropertiesUtils.readVersionProperty
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * The KotlinBasePlugin is a plugin that is used for shared configuration between [KotlinProjectPlugin]
 * and [KotlinLibraryPlugin].
 */
class KotlinBasePlugin: Plugin<Project> {
    override fun apply(project: Project) {
        with(project.pluginManager) {
            apply(CommonKotlinPlugin::class.java) // Inherit the base configuration from the BasePlugin.
        }

        with(project.dependencies) {
            add("testImplementation", "org.jetbrains.kotlin:kotlin-test") // Jacoco depends on "test" task
        }
    }
}
