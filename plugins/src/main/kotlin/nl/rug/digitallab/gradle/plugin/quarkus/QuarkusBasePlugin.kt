package nl.rug.digitallab.gradle.plugin.quarkus

import io.quarkus.gradle.QuarkusPlugin
import nl.rug.digitallab.gradle.plugin.common.CommonKotlinPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.jetbrains.kotlin.allopen.gradle.AllOpenExtension
import org.jetbrains.kotlin.allopen.gradle.AllOpenGradleSubplugin

/**
 * The QuarkusBasePlugin is a plugin that is used for shared configuration between [QuarkusProjectPlugin]
 * and [QuarkusLibraryPlugin].
 */
class QuarkusBasePlugin: Plugin<Project> {
    override fun apply(project: Project) {
        with(project.pluginManager) {
            // Apply the plugins specific to Quarkus projects or libraries.
            apply(CommonKotlinPlugin::class.java)
            apply(AllOpenGradleSubplugin::class.java)
            apply(QuarkusPlugin::class.java)
        }

        with(project.dependencies) {
            // Add the default Quarkus dependencies that every base Quarkus project uses.
            add("implementation", "io.quarkus:quarkus-arc") // CDI
            add("implementation", "io.quarkus:quarkus-kotlin") // Kotlin support
            add("implementation", "io.quarkus:quarkus-config-yaml") // YAML configuration is preferred over JSON

            add("testImplementation", "io.quarkus:quarkus-junit5") // Quarkus JUnit support
            add("testImplementation", "io.quarkus:quarkus-jacoco") // Code coverage reports
        }

        with(project.extensions) {
            // Set up the AllOpen extension. This is needed because Kotlin classes are final by default. Final classes
            // cannot be proxied by CDI, which is a requirement for Quarkus.
            configure(AllOpenExtension::class.java) {
                it.annotation("jakarta.ws.rs.Path")
                it.annotation("jakarta.enterprise.context.ApplicationScoped")
                it.annotation("jakarta.persistence.Entity")
                it.annotation("io.quarkus.test.junit.QuarkusTest")
            }
        }

        with(project.tasks) {
            // Set up the 'test' task
            withType(Test::class.java).configureEach {
                it.systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")

                // Pass Quarkus system properties to the testing JVM.
                System.getProperties().forEach { key, value ->
                    if (key.toString().startsWith("quarkus."))
                        it.systemProperty(key.toString(), value.toString())
                }

                // Fixes https://github.com/quarkusio/quarkus/issues/35008
                it.outputs.file(project.layout.buildDirectory.file("jacoco-report/jacoco.xml"))
            }
        }
    }
}
