package nl.rug.digitallab.gradle.plugin.quarkus.project

import nl.rug.digitallab.gradle.plugin.quarkus.QuarkusBasePlugin
import nl.rug.digitallab.gradle.util.VersionPropertiesUtils.readVersionProperty
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * The QuarkusProjectPlugin is a plugin that is used to configure a Quarkus project.
 */
class QuarkusProjectPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        val quarkusVersion = project.readVersionProperty("quarkusVersion")

        with(project.pluginManager) {
            apply(QuarkusBasePlugin::class.java)
        }

        with(project.dependencies) {
            // Add the default Quarkus dependencies that every Quarkus project uses.
            add("implementation", enforcedPlatform("io.quarkus.platform:quarkus-bom:$quarkusVersion"))
            add("implementation", "io.quarkus:quarkus-micrometer-registry-prometheus") // Metrics endpoint
            add("implementation", "io.quarkus:quarkus-opentelemetry") // OpenTelemetry support
            add("implementation", "io.quarkus:quarkus-smallrye-health") // Health check endpoint
        }
    }
}
