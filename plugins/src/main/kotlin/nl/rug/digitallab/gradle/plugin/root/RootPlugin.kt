package nl.rug.digitallab.gradle.plugin.root

import nl.rug.digitallab.gradle.plugin.common.CommonRootPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * The [RootPlugin] can be applied to the root project of a multi-module gradle project. Typically we avoid having a
 * root build.gradle.kts file, however if it is unavoidable then this plugin can be applied to the root project.
 */
class RootPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        project.pluginManager.apply(CommonRootPlugin::class.java)
    }
}
