package nl.rug.digitallab.gradle.plugin.kotlin.project

import nl.rug.digitallab.gradle.plugin.kotlin.KotlinBasePlugin
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * The KotlinProjectPlugin is a plugin that is used to configure a Kotlin project.
 */
class KotlinProjectPlugin: Plugin<Project> {
    override fun apply(project: Project) = with(project.pluginManager) {
        // Apply the non-framework specific configuration
        apply(KotlinBasePlugin::class.java)
    }
}
