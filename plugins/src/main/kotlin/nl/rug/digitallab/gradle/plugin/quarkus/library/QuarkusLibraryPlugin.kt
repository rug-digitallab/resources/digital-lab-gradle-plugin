package nl.rug.digitallab.gradle.plugin.quarkus.library

import io.quarkus.gradle.tasks.QuarkusBuildDependencies
import nl.rug.digitallab.gradle.plugin.common.publish.PublishablePlugin
import nl.rug.digitallab.gradle.plugin.quarkus.QuarkusBasePlugin
import nl.rug.digitallab.gradle.util.VersionPropertiesUtils.readVersionProperty
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.kordamp.gradle.plugin.jandex.JandexPlugin

/**
 * The QuarkusLibraryProjectPlugin is a plugin that is used to configure a Quarkus library project.
 */
class QuarkusLibraryPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        val quarkusVersion = project.readVersionProperty("quarkusVersion")

        with(project.pluginManager) {
            apply(JandexPlugin::class.java)
            apply(PublishablePlugin::class.java)
            apply(QuarkusBasePlugin::class.java)
        }

        with(project.dependencies) {
            add("implementation", platform("io.quarkus.platform:quarkus-bom:$quarkusVersion"))
        }

        with(project.tasks) {
            // Gradle needs the dependencies between tasks to be declared explicitly, jandex does not do so.
            withType(QuarkusBuildDependencies::class.java).configureEach {
                it.dependsOn("jandex")
            }

            withType(Test::class.java).configureEach {
                it.dependsOn("jandex")
            }
        }
    }
}
