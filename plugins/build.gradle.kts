dependencies {
    val kotlinVersion: String by project
    val quarkusVersion: String by project
    val jandexVersion: String by project

    implementation(project(":common"))
    implementation("io.quarkus:gradle-application-plugin:$quarkusVersion")
    implementation("org.jetbrains.kotlin:kotlin-allopen:$kotlinVersion")
    implementation("org.kordamp.gradle:jandex-gradle-plugin:$jandexVersion")
}

gradlePlugin {
    plugins {
        create("RootPlugin") {
            id = "nl.rug.digitallab.gradle.plugin.root"
            implementationClass = "nl.rug.digitallab.gradle.plugin.root.RootPlugin"
        }

        create("KotlinProject") {
            id = "nl.rug.digitallab.gradle.plugin.kotlin.project"
            implementationClass = "nl.rug.digitallab.gradle.plugin.kotlin.project.KotlinProjectPlugin"
        }

        create("KotlinLibrary") {
            id = "nl.rug.digitallab.gradle.plugin.kotlin.library"
            implementationClass = "nl.rug.digitallab.gradle.plugin.kotlin.library.KotlinLibraryPlugin"
        }

        create("JavaProject") {
            id = "nl.rug.digitallab.gradle.plugin.java.project"
            implementationClass = "nl.rug.digitallab.gradle.plugin.java.project.JavaProjectPlugin"
        }

        create("JavaLibrary") {
            id = "nl.rug.digitallab.gradle.plugin.java.library"
            implementationClass = "nl.rug.digitallab.gradle.plugin.java.library.JavaLibraryPlugin"
        }

        create("QuarkusProject") {
            id = "nl.rug.digitallab.gradle.plugin.quarkus.project"
            implementationClass = "nl.rug.digitallab.gradle.plugin.quarkus.project.QuarkusProjectPlugin"
        }

        create("QuarkusLibrary") {
            id = "nl.rug.digitallab.gradle.plugin.quarkus.library"
            implementationClass = "nl.rug.digitallab.gradle.plugin.quarkus.library.QuarkusLibraryPlugin"
        }
    }
}
