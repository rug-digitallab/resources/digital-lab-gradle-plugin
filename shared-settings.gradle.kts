// This shared settings.gradle.kts is applied to both the root project and the buildSrc
pluginManagement {
    val kotlinVersion: String by settings
    val gradlePluginPublishVersion: String by settings
    val sonarqubeVersion: String by settings

    plugins {
        kotlin("jvm") version kotlinVersion
        id("com.gradle.plugin-publish") version gradlePluginPublishVersion
        id("org.sonarqube") version sonarqubeVersion
    }

    repositories {
        mavenCentral()
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Group Repository
        gradlePluginPortal()
    }
}
