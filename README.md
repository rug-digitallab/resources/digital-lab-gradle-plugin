# Digital Lab Gradle Plugin
The Digital Lab Gradle Plugin contains multiple gradle plugins which provide default configuration and tasks for
Digital Lab projects. The following plugins are published:

- Kotlin Project Plugin (`KotlinProject`, id: `nl.rug.digitallab.gradle.plugin.kotlin.project`)
- Kotlin Library Plugin (`KotlinLibrary`, id: `nl.rug.digitallab.gradle.plugin.kotlin.library`)
- Quarkus Project Plugin (`QuarkusProject`, id: `nl.rug.digitallab.gradle.plugin.quarkus.project`)
- Quarkus Library Plugin (`QuarkusLibrary`, id: `nl.rug.digitallab.gradle.plugin.quarkus.library`)
- Java Project Plugin (`JavaProject`, id: `nl.rug.digitallab.gradle.plugin.java.project`)
- Java Library Plugin (`JavaLibrary`, id: `nl.rug.digitallab.gradle.plugin.java.library`)

If you are developing a Kotlin/Quarkus/Java project, use their respective project plugins. If you are developing a Kotlin/Quarkus/Java
library, use their respective library plugins. The library plugins support publication to maven repositories.

# Prerequisites
To use the Digital Lab Gradle Plugin, you need to have the following installed:

- Java 21 or higher
- Gradle 8.x

A checkout of this repository on Windows requires `core.symlinks=true` to be set in your git configuration. Additionally,
you may need to run `git` with administrative privileges.

# Usage
To use the Digital Lab Gradle Plugin, you need to add the following to your `settings.gradle.kts` file:

```kotlin
pluginManagement {
    val digitalLabGradlePluginVersion: String by settings
    plugins {
        id("nl.rug.digitallab.gradle.plugin.xxxx.xxxx") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Group Repository
        gradlePluginPortal()
    }
}
```

Your `gradle.properties` file should contain the following:

```properties
digitalLabGradlePluginVersion=0.0.2-SNAPSHOT
```

To apply the plugin to a project, you need to add the following to your `build.gradle.kts` file:

```kotlin
plugins {
    id("nl.rug.digitallab.gradle.plugin.xxxx.xxxx")
}
```

And finally, create a `VERSION` file in the root of your project with a valid semantic version.

## Version Plugin

The Version plugin is applied to all projects using any of the Digital Lab Gradle plugins. The plugin enforces that
the version is always a valid semantic version. It also reads the version from a `VERSION` file if present. If no `VERSION`
file is present, an error will be thrown. The version in the `VERSION` file should be a valid semantic version and should
start at `0.0.1-SNAPSHOT`. The plugin also adds the `printVersion` task to print the version to the console.

## Git Plugin

The Git plugin is applied to all projects using any of the Digital Lab Gradle plugins. The plugin contains the `startRelease`
task to start a new release. It automatically bumps the version in the VERSION file and creates a new release branch. After
running this task, the last step is to push both `develop` and the new `release/x.y.z` branch to the remote repository.

If you want to use a specific version, you can use the following command:

```shell
./gradlew -PtargetDevelopVersion=0.3.0-SNAPSHOT startRelease
```

This will set the release version to current VERSION (without -SNAPSHOT) and the new develop version to 0.3.0-SNAPSHOT.
The  -PtargetDevelopVersion is optional.

## Publishable Plugin

To use the publishable plugin, add the following to the GitLab CI pipeline:

```yaml
package:
  stage: publish
  script:
    - $GRADLE_CLI publish -PmavenRepositoryName=GitLab -PmavenRepositoryUrl=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/maven -PmavenRepositoryHeaderName=Job-Token -PmavenRepositoryHeaderValue=$CI_JOB_TOKEN
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
      when: on_success
    - if: $CI_COMMIT_TAG
      when: on_success
```

It is also possible to release to the local maven repository using `./gradlew publishToMavenLocal`. When applied to a
gradle plugin, the publishable plugin will attempt to detect this and not add additional publications. This is necessary
to prevent duplicate publications to the maven repository. If the publishable plugin does not automatically detect this,
you can set the following property to `true` in your `gradle.properties` file:

```properties
isGradlePlugin=true
```

# Internals

The Digital Lab Gradle Plugin is a multi-project gradle build consisting of the following subprojects:

- `common` - Contains common plugins and tasks that should also be applied to the Digital Lab Gradle Plugin project itself.
- `plugins` - Contains the Kotlin/Quarkus Project and Library plugins, as well as a Root plugin.
- `test-suite` - Contains the tests for the Digital Lab Gradle Plugin.

Additionally, the Digital Lab Gradle Plugin also defines a `buildSrc`, its only goal is to build the `common` sources
and apply them to the Digital Lab Gradle Plugin itself, such that this project can also make use of the default configurations and
plugins.

## Common
The `common` subproject consists of several plugins that are not published but applied in the `plugins` module:

- `CommonPlugin` - This plugin is applied to all the published plugins and contains common configuration and tasks. The
  `CommonPlugin` also applies the `GitPlugin` and `VersionPlugin`. This plugin will not apply any of Kotlin specific plugins.
- `CommonKotlinPlugin` - This plugin is applied to all the published Kotlin and Quarkus plugins and contains Kotlin-specific configuration and tasks. The `CommonKotlinPlugin` also applies the `CommonPlugin`
- `CommonJavaPlugin` - This plugin is applied to all the published Java plugins and contains Java-specific configuration and tasks. The `CommonJavaPlugin` also applies the `CommonPlugin`
- `CommonRootPlugin` - This plugin is applied to the root project and contains common configuration and tasks.
- `GitPlugin` - This plugin contains the `startRelease` task to start a new release.
- `PublishablePlugin` - This plugin configures the `maven-publish` plugin and ensures the source code and javadoc are
  published too.
- `VersionPlugin` - This plugin enforces that the version is always a valid semantic version. It also reads the version
  from a VERSION file. If no VERSION file is present, an error will be thrown. Adds the `printVersion` task to print the
  version to the console.

NOTE: If you add any dependencies to the `build.gradle.kts` in the `common` module, it must also be added to the `build.gradle.kts` of
the `buildSrc` module.

## Plugins
The `plugins` subproject consists of several plugins that are published:

- `KotlinProject` - This plugin is applied to Kotlin projects and contains common configuration.
- `KotlinLibrary` - This plugin is applied to Kotlin libraries and contains common configuration.
- `QuarkusProject` - This plugin is applied to Quarkus projects and contains common configuration.
- `QuarkusLibrary` - This plugin is applied to Quarkus libraries and contains common configuration.
- `JavaProject` - This plugin is applied to Java projects and contains common configuration.
- `JavaLibrary` - This plugin is applied to Java libraries and contains common configuration.
- `RootPlugin` - This plugin is applied to the root project and contains common configuration.

## Tests
The `test-suite` subproject adds all other plugins to its classpath and uses the `GradleRunner` from the `gradle-test-kit` to
test all the plugins.
