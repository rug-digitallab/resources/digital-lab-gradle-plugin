dependencies {
    val junitVersion: String by project
    val kotestVersion: String by project

    implementation("org.junit.jupiter:junit-jupiter:$junitVersion")
    implementation("io.kotest:kotest-assertions-core:$kotestVersion")
    implementation(gradleTestKit())

    // Adds the Gradle plugins to the classpath so can be used by SimplePluginTester
    implementation(project(":common"))
    implementation(project(":plugins"))
}
