package nl.rug.digitallab.gradle.plugin.test.quarkus

import nl.rug.digitallab.gradle.plugin.test.PluginTest
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testDependencyAbsence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testDependencyPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import nl.rug.digitallab.gradle.plugin.test.common.CommonPluginTest
import org.junit.jupiter.api.Test

/**
 * "Trait"-like interface implementing tests for any Quarkus plugin
 */
interface QuarkusGradlePluginTest : PluginTest, CommonPluginTest {
    @Test
    fun `Test Quarkus runtime dependency list`() {
        testDependencyPresence(listOf(
            "io.quarkus:quarkus-arc",
            "io.quarkus:quarkus-kotlin",
            "io.quarkus:quarkus-config-yaml",
        ))
    }

    @Test
    fun `Test Quarkus test dependency list`() {
        testDependencyPresence(listOf(
            "io.quarkus:quarkus-junit5",
            "io.quarkus:quarkus-jacoco",
        ), "testCompileClasspath")
    }

    @Test
    fun `Test Quarkus test dependencies opt-out`() {
        appendProperties("""
            includeCommonLibraries=false
        """.trimIndent())

        testDependencyAbsence(listOf(
            "nl.rug.digitallab.common.quarkus:test",
        ), "testCompileClasspath")
    }

    @Test
    fun `Test Quarkus expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "org.jetbrains.kotlin.allopen.gradle.AllOpenGradleSubplugin",
                "io.quarkus.gradle.QuarkusPlugin",
            ),
        )
    }
}
