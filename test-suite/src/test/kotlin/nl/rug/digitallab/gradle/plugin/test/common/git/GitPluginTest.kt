package nl.rug.digitallab.gradle.plugin.test.common.git

import nl.rug.digitallab.gradle.plugin.test.PluginTest
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testTaskPresence
import org.junit.jupiter.api.Test

interface GitPluginTest : PluginTest {
    @Test
    fun `Test presence of Git release task`() {
        testTaskPresence(listOf("startRelease"))
    }
}
