package nl.rug.digitallab.gradle.plugin.test.kotlin.library

import nl.rug.digitallab.gradle.plugin.test.common.publish.PublishablePluginTest
import nl.rug.digitallab.gradle.plugin.test.kotlin.KotlinGradlePluginTest
import nl.rug.digitallab.gradle.plugin.test.SimplePluginTester
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import org.junit.jupiter.api.Test

/**
 * Tests for [KotlinLibraryPlugin]
 */
class KotlinLibraryPluginTest : SimplePluginTester(), KotlinGradlePluginTest, PublishablePluginTest {
    override val pluginName = "nl.rug.digitallab.gradle.plugin.kotlin.library"

    @Test
    fun `Test Kotlin library expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "nl.rug.digitallab.gradle.plugin.kotlin.KotlinBasePlugin",
                "nl.rug.digitallab.gradle.plugin.common.publish.PublishablePlugin",
            ),
        )
    }
}
