package nl.rug.digitallab.gradle.plugin.test.quarkus.project

import nl.rug.digitallab.gradle.plugin.test.quarkus.QuarkusGradlePluginTest
import nl.rug.digitallab.gradle.plugin.test.SimplePluginTester
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testDependencyPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import org.junit.jupiter.api.Test

/**
 * Tests for the [QuarkusProjectPlugin]
 */
class QuarkusProjectPluginTest : SimplePluginTester(), QuarkusGradlePluginTest {
    override val pluginName = "nl.rug.digitallab.gradle.plugin.quarkus.project"

    @Test
    fun `Test enforced Quarkus BOM version injection`() {
        val version = "3.18.4"

        appendProperties("""
            quarkusVersion=$version
        """.trimIndent())

        testDependencyPresence(listOf(
            "io.quarkus.platform:quarkus-bom:{strictly $version}",
        ))
    }

    @Test
    fun `Test Quarkus project dependency list`() {
        testDependencyPresence(listOf(
            "io.quarkus:quarkus-micrometer-registry-prometheus",
            "io.quarkus:quarkus-opentelemetry",
            "io.quarkus:quarkus-smallrye-health",
        ), "testCompileClasspath")
    }

    @Test
    fun `Test Quarkus project expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "nl.rug.digitallab.gradle.plugin.quarkus.QuarkusBasePlugin",
            ),
        )
    }
}
