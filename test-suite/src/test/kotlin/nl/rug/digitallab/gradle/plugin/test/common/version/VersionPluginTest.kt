package nl.rug.digitallab.gradle.plugin.test.common.version

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.string.shouldContain
import nl.rug.digitallab.gradle.plugin.test.PluginTest
import org.gradle.internal.impldep.org.junit.After
import org.gradle.testkit.runner.UnexpectedBuildFailure
import org.junit.jupiter.api.Test

/**
 * "Trait"-like interface implementing a test for any plugin using [VersionPlugin]
 */
interface VersionPluginTest : PluginTest {
    @Test
    fun `Valid semantic version should be reported correctly`() {
        val version = "0.0.1-SNAPSHOT"

        writeSettings(simpleGradleSettings)
        writeBuild(simpleGradleBuild)
        writeVersion(version)
        appendProperties(simpleGradleProperties)

        val result = runGradle("printVersion")
        
        result.output.shouldContain(version)
    }

    @Test
    fun `Non-semantic versions should result in an exception`() {
        val version = "invalid-semantic-version"

        writeSettings(simpleGradleSettings)
        writeBuild(simpleGradleBuild)
        writeVersion(version)
        appendProperties(simpleGradleProperties)

        shouldThrow<UnexpectedBuildFailure> { runGradle("printVersion") }
    }

    @After
    fun `Restore VERSION file to valid format`() {
        writeVersion("0.0.1-SNAPSHOT")
    }
}
