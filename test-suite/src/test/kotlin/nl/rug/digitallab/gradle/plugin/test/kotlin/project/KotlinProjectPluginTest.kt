package nl.rug.digitallab.gradle.plugin.test.kotlin.project

import nl.rug.digitallab.gradle.plugin.test.kotlin.KotlinGradlePluginTest
import nl.rug.digitallab.gradle.plugin.test.SimplePluginTester
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import org.junit.jupiter.api.Test

/**
 * Tests for [KotlinProjectPlugin]
 */
class KotlinProjectPluginTest : SimplePluginTester(), KotlinGradlePluginTest {
    override val pluginName = "nl.rug.digitallab.gradle.plugin.kotlin.project"

    @Test
    fun `Test Kotlin project expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "nl.rug.digitallab.gradle.plugin.kotlin.KotlinBasePlugin",
            ),
        )
    }
}
