package nl.rug.digitallab.gradle.plugin.test.common.publish

import nl.rug.digitallab.gradle.plugin.test.PluginTest
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testTaskPresence
import org.junit.jupiter.api.Test

/**
 * "Trait"-like interface implementing tests for any plugin using [PublishablePlugin]
 */
interface PublishablePluginTest : PluginTest {
    @Test
    fun `Test presence of Dokka tasks`() {
        testTaskPresence(listOf(
            "dokkaHtmlJar",
            "dokkaJavadocJar",
        ))
    }

    @Test
    fun `Test presence of publishing tasks`() {
        testTaskPresence(listOf(
            "publish",
            "publishFooPublicationToMavenLocal",
        ))
    }

    @Test
    fun `Test publishable expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "org.jetbrains.dokka.gradle.DokkaPlugin",
                "org.gradle.api.publish.maven.plugins.MavenPublishPlugin_Decorated",
            ),
        )
    }
}
