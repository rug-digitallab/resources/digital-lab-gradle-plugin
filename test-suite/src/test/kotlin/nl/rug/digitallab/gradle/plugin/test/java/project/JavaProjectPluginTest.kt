package nl.rug.digitallab.gradle.plugin.test.java.project

import nl.rug.digitallab.gradle.plugin.test.java.JavaGradlePluginTest
import nl.rug.digitallab.gradle.plugin.test.SimplePluginTester
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import org.junit.jupiter.api.Test

/**
 * Tests for [JavaProjectPlugin]
 */
class JavaProjectPluginTest : SimplePluginTester(), JavaGradlePluginTest {
    override val pluginName = "nl.rug.digitallab.gradle.plugin.java.project"

    @Test
    fun `Test Java project expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "nl.rug.digitallab.gradle.plugin.java.JavaBasePlugin",
            ),
        )
    }
}
