package nl.rug.digitallab.gradle.plugin.test.quarkus.library

import nl.rug.digitallab.gradle.plugin.test.common.publish.PublishablePluginTest
import nl.rug.digitallab.gradle.plugin.test.quarkus.QuarkusGradlePluginTest
import nl.rug.digitallab.gradle.plugin.test.SimplePluginTester
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testDependencyPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import org.junit.jupiter.api.Test

/**
 * Tests for the [QuarkusLibraryPlugin]
 */
class QuarkusLibraryPluginTest : SimplePluginTester(), QuarkusGradlePluginTest, PublishablePluginTest {
    override val pluginName = "nl.rug.digitallab.gradle.plugin.quarkus.library"

    @Test
    fun `Test Quarkus BOM version injection`() {
        val version = "3.18.4"

        appendProperties("""
            quarkusVersion=$version
        """.trimIndent())

        testDependencyPresence(listOf(
            "io.quarkus.platform:quarkus-bom:$version",
        ))
    }

    @Test
    fun `Test Quarkus library expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "org.kordamp.gradle.plugin.jandex.JandexPlugin",
                "nl.rug.digitallab.gradle.plugin.quarkus.QuarkusBasePlugin",
                "nl.rug.digitallab.gradle.plugin.common.publish.PublishablePlugin",
            ),
        )
    }
}
