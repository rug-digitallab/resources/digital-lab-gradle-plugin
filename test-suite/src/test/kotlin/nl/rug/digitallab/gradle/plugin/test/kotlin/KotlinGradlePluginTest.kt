package nl.rug.digitallab.gradle.plugin.test.kotlin

import nl.rug.digitallab.gradle.plugin.test.PluginTest
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testDependencyPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testTaskPresence
import nl.rug.digitallab.gradle.plugin.test.common.CommonPluginTest
import org.junit.jupiter.api.Test

/**
 * "Trait"-like interface implementing tests for any plugin using [KotlinBasePlugin]
 */
interface KotlinGradlePluginTest : PluginTest, CommonPluginTest {
    @Test
    fun `Test presence of Jacoco tasks`() {
        testTaskPresence(listOf(
            "jacocoTestReport",
        ))
    }

    @Test
    fun `Test Kotlin test dependency list`() {
        testDependencyPresence(listOf(
            "org.junit.jupiter:junit-jupiter",
        ), "testCompileClasspath")
    }

    @Test
    fun `Test Kotlin expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "nl.rug.digitallab.gradle.plugin.common.CommonPlugin",
                "org.gradle.testing.jacoco.plugins.JacocoPlugin_Decorated",
            ),
        )
    }
}
