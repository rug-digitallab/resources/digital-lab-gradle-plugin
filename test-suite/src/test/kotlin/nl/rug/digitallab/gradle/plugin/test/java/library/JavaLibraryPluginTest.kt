package nl.rug.digitallab.gradle.plugin.test.java.library

import nl.rug.digitallab.gradle.plugin.test.common.publish.PublishablePluginTest
import nl.rug.digitallab.gradle.plugin.test.java.JavaGradlePluginTest
import nl.rug.digitallab.gradle.plugin.test.SimplePluginTester
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import org.junit.jupiter.api.Test

/**
 * Tests for [JavaLibraryPlugin]
 */
class JavaLibraryPluginTest : SimplePluginTester(), JavaGradlePluginTest, PublishablePluginTest {
    override val pluginName = "nl.rug.digitallab.gradle.plugin.java.library"

    @Test
    fun `Test Java library expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins =
                listOf(
                "nl.rug.digitallab.gradle.plugin.java.JavaBasePlugin",
                "nl.rug.digitallab.gradle.plugin.common.publish.PublishablePlugin",
            ),
        )
    }
}
