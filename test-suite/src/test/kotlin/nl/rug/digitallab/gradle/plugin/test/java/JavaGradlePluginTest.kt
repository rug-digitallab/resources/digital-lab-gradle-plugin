package nl.rug.digitallab.gradle.plugin.test.java

import nl.rug.digitallab.gradle.plugin.test.PluginTest
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testDependencyPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testTaskPresence
import nl.rug.digitallab.gradle.plugin.test.common.git.GitPluginTest
import nl.rug.digitallab.gradle.plugin.test.common.version.VersionPluginTest
import org.junit.jupiter.api.Test

/**
 * "Trait"-like interface implementing tests for any plugin using [JavaBasePlugin]
 */
interface JavaGradlePluginTest : PluginTest, VersionPluginTest, GitPluginTest {
    @Test
    fun `Test presence of Jacoco tasks`() {
        testTaskPresence(listOf(
            "jacocoTestReport",
        ))
    }

    @Test
    fun `Test Java test dependency list`() {
        testDependencyPresence(listOf(
            "org.junit.jupiter:junit-jupiter",
        ), "testCompileClasspath")
    }

    @Test
    fun `Test Java expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins= listOf(
                "org.gradle.testing.jacoco.plugins.JacocoPlugin_Decorated",
            ),
        )
    }
}
