package nl.rug.digitallab.gradle.plugin.test.common

import nl.rug.digitallab.gradle.plugin.common.CommonPlugin
import nl.rug.digitallab.gradle.plugin.test.PluginTest
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testDependencyAbsence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testDependencyPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testTaskPresence
import nl.rug.digitallab.gradle.plugin.test.common.git.GitPluginTest
import nl.rug.digitallab.gradle.plugin.test.common.version.VersionPluginTest
import org.junit.jupiter.api.Test

interface CommonPluginTest: PluginTest, CommonRootPluginTest, VersionPluginTest, GitPluginTest {
    @Test
    fun `Test common dependency list`() {
        testDependencyPresence(listOf(
            "org.jetbrains.kotlin:kotlin-stdlib",
            "nl.rug.digitallab.common.kotlin:quantities",
        ))
    }

    @Test
    fun `Test common test dependency list`() {
        testDependencyPresence(listOf(
            "io.kotest:kotest-assertions-core"
        ), "testCompileClasspath")
    }

    @Test
    fun `Test common dependencies opt-out`() {
        appendProperties("""
            includeCommonLibraries=false
        """.trimIndent())

        testDependencyAbsence(listOf(
            "nl.rug.digitallab.common.kotlin:quantities",
        ))
    }

    @Test
    fun `Test common expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "org.gradle.api.plugins.JvmEcosystemPlugin_Decorated",
                "org.jetbrains.kotlin.noarg.gradle.NoArgGradleSubplugin",
                "org.jetbrains.kotlin.noarg.gradle.KotlinJpaSubplugin",
                CommonPlugin::class.java.name,
            ),
        )
    }

    @Test
    fun `Test presence of expected tasks`() {
        testTaskPresence(listOf("sonar"))
        testTaskPresence(listOf("generateLock", "saveLock"))
    }
}
