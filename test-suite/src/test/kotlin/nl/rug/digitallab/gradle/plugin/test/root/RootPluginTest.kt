package nl.rug.digitallab.gradle.plugin.test.root

import nl.rug.digitallab.gradle.plugin.test.SimplePluginTester
import nl.rug.digitallab.gradle.plugin.test.common.CommonRootPluginTest

/**
 * Tests for the [RootPlugin]
 */
class RootPluginTest: SimplePluginTester(), CommonRootPluginTest {
    override val pluginName = "nl.rug.digitallab.gradle.plugin.root"
}
