package nl.rug.digitallab.gradle.plugin.test.common

import nl.rug.digitallab.gradle.plugin.test.PluginTest
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testPluginPresence
import nl.rug.digitallab.gradle.plugin.test.StandardPluginTests.testTaskPresence
import nl.rug.digitallab.gradle.plugin.test.common.git.GitPluginTest
import nl.rug.digitallab.gradle.plugin.test.common.version.VersionPluginTest
import org.junit.jupiter.api.Test

interface CommonRootPluginTest: PluginTest, VersionPluginTest, GitPluginTest {
    @Test
    fun `Test common root expected plugins`() {
        testPluginPresence(
            projectName = "foo",
            plugins = listOf(
                "nl.rug.digitallab.gradle.plugin.common.version.VersionPlugin",
                "nl.rug.digitallab.gradle.plugin.common.git.GitPlugin",
                "org.sonarqube.gradle.SonarQubePlugin",
            ),
        )
    }

    @Test
    fun `Test presence of common root expected tasks`() {
        testTaskPresence(listOf(
            "printVersion",
            "sonar",
            "startRelease",
        ))
    }
}
