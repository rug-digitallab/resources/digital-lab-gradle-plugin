package nl.rug.digitallab.gradle.plugin.test

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.io.FileWriter

/**
 * Helper class to define basic tooling for performing tests on the
 * Gradle plugins. Provides a basic test project structure.
 */
abstract class SimplePluginTester : PluginTest {
    /**
     * Package name of the plugin under test
     */
    protected abstract val pluginName: String

    // We will work in a temporary directory
    @field:TempDir
    lateinit var projectDirectory: File

    // Define the basic project files we should write to
    private lateinit var projectSettings: File
    private lateinit var projectBuild: File
    private lateinit var projectProperties: File
    private lateinit var projectVersion: File

    // Default, simple Gradle project settings and build file we use for the majority of tests.
    override val simpleGradleSettings = """
        rootProject.name = "foo"
    """.trimIndent()
    override val simpleGradleBuild
        get() = """
            plugins {
                id("$pluginName")
            }
        """.trimIndent()
    override val simpleGradleProperties = "projectGroupId=nl.rug.digitallab.gradle.plugin"
    override val simpleVersion = "0.0.1-SNAPSHOT"

    @BeforeEach
    fun setupFiles() {
        projectSettings = File(projectDirectory, "settings.gradle.kts")
        projectBuild = File(projectDirectory, "build.gradle.kts")
        projectProperties = File(projectDirectory, "gradle.properties")
        projectVersion = File(projectDirectory, "VERSION")
    }

    // Helper functions to populate Gradle project files
    override fun writeProjectFile(filename: String, content: String) {
        FileWriter(File(projectDirectory, filename)).use { it.write(content) }
    }
    override fun writeSettings(gradleSettings: String) {
        FileWriter(projectSettings).use { it.write(gradleSettings) }
    }
    override fun writeBuild(gradleBuild: String) {
        FileWriter(projectBuild).use { it.write(gradleBuild) }
    }
    override fun writeProperties(gradleProperties: String) {
        FileWriter(projectProperties).use { it.write(gradleProperties) }
    }
    override fun appendProperties(gradleProperties: String) {
        FileWriter(projectProperties, true).use { it.write(String.format("%s%n", gradleProperties)) }
    }
    override fun writeVersion(version: String) {
        FileWriter(projectVersion).use { it.write(version) }
    }

    // Helper to run Gradle on the project files
    override fun runGradle(vararg arguments: String, configure: GradleRunner.() -> GradleRunner): BuildResult {
        return GradleRunner.create()
            .withProjectDir(projectDirectory)
            .withArguments(arguments.toList())
            .withPluginClasspath()
            .configure()
            .build()
    }
}
