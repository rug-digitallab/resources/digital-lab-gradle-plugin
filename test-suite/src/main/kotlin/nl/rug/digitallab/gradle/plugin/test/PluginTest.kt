package nl.rug.digitallab.gradle.plugin.test

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner

/**
 * This interface defines some utility functions that need to be available for
 * any test set of a plugin. All plugin-specific tests are implemented in another
 * interface, in a "trait"-like fashion. These interfaces should inherit this one,
 * so they can use these helper methods in their implementation. The actual test
 * class instance will then have implementations of these functions for runtime.
 */
interface PluginTest {
    /**
     * Simple Gradle Settings script for a minimal project for testing
     */
    val simpleGradleSettings: String

    /**
     * Simple Gradle Build script for a minimal project for testing
     */
    val simpleGradleBuild: String

    /**
     * Simple Gradle Properties definitions for a minimal project for testing
     */
    val simpleGradleProperties: String

    /**
     * Simple version string for a minimal project for testing
     */
    val simpleVersion: String

    /**
     * Helper function to write an auxiliary file to the test project
     *
     * @param filename Name of the file to write
     * @param content Contents of the file to write
     */
    fun writeProjectFile(filename: String, content: String)

    /**
     * Helper function to write out the `settings.gradle.kts` file.
     *
     * @param gradleSettings Gradle Settings script
     */
    fun writeSettings(gradleSettings: String)

    /**
     * Helper function to write out the `build.gradle.kts` file.
     *
     * @param gradleBuild Gradle Build script
     */
    fun writeBuild(gradleBuild: String)

    /**
     * Helper function to write out the `gradle.properties` file.
     *
     * @param gradleProperties Gradle Properties definitions
     */
    fun writeProperties(gradleProperties: String)

    /**
     * Helper function to append to the `gradle.properties` file.
     *
     * @param gradleProperties Gradle Properties definitions to append
     */
    fun appendProperties(gradleProperties: String)

    /**
     * Helper function to write out the `VERSION` file.
     *
     * @param version Version string to write
     */
    fun writeVersion(version: String)

    /**
     * Run Gradle on the current configuration. Customization of the runner
     * is possible through the optional lambda function parameter.
     *
     * @param arguments List of arguments to send to the Gradle CLI
     * @param configure Optional custom configuration provider, to customize the [GradleRunner]
     *
     * @return [BuildResult] Result from [GradleRunner]
     */
    fun runGradle(vararg arguments: String, configure: GradleRunner.() -> GradleRunner = { this }): BuildResult
}
