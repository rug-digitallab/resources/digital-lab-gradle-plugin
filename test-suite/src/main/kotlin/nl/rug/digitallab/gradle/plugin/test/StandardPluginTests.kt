package nl.rug.digitallab.gradle.plugin.test

import io.kotest.inspectors.forAll
import io.kotest.inspectors.forOne
import io.kotest.matchers.string.shouldContain
import io.kotest.matchers.string.shouldContainOnlyOnce
import io.kotest.matchers.string.shouldNotContain
import io.kotest.matchers.string.shouldStartWith

/**
 * Helper functions implementing common test patterns
 */
object StandardPluginTests {
    /**
     * Helper function implementing a standard task presence test
     *
     * @param tasks List of task names to check for presence
     */
    fun PluginTest.testTaskPresence(tasks: List<String>) {
        writeSettings(simpleGradleSettings)
        writeBuild(simpleGradleBuild)
        writeVersion(simpleVersion)
        appendProperties(simpleGradleProperties)

        val result = runGradle("tasks")

        tasks.forAll { task ->
            // To match only exact task names, we ensure that in the output, there
            // is exactly one line that starts with the task name as a single word.
            // This way, we don't have false positives for task names that are in
            // the description of another task, or are an extension of the task we
            // are looking for.
            // For example, "publish" would also match "publishToMavenLocal", which
            // is not desirable.
            result.output.lines().forOne { it.shouldStartWith(Regex("$task\\b")) }
        }
    }

    /**
     * Helper function implementing a standard dependency presence test
     *
     * @param dependencies List of dependencies to check for presence
     * @param classpath Name of the classpath to check
     */
    fun PluginTest.testDependencyPresence(dependencies: List<String>, classpath: String = "compileClasspath") {
        writeSettings(simpleGradleSettings)
        writeBuild(simpleGradleBuild)
        writeVersion(simpleVersion)
        appendProperties(simpleGradleProperties)

        val result = runGradle("dependencies", "--configuration", classpath)

        dependencies.forAll {
            result.output.shouldContain(it)
        }
    }

    /**
     * Helper function implementing a standard dependency absence test
     *
     * @param dependencies List of dependencies to check for absence
     * @param classpath Name of the classpath to check
     */
    fun PluginTest.testDependencyAbsence(dependencies: List<String>, classpath: String = "compileClasspath") {
        writeSettings(simpleGradleSettings)
        writeBuild(simpleGradleBuild)
        writeVersion(simpleVersion)
        appendProperties(simpleGradleProperties)

        val result = runGradle("dependencies", "--configuration", classpath)

        dependencies.forAll {
            result.output.shouldNotContain(it)
        }
    }

    /**
     * Helper function implementing a standard plugin presence test
     *
     * @param plugins List of plugins to check for presence
     */
    fun PluginTest.testPluginPresence(projectName: String, plugins: List<String>) {
        writeSettings(simpleGradleSettings)
        writeBuild(simpleGradleBuild)
        writeVersion(simpleVersion)
        appendProperties(simpleGradleProperties)

        val result = runGradle("listPlugins")

        plugins.forAll {
            result.output.shouldContainOnlyOnce("$projectName:$it")
        }
    }
}
