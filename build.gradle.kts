import nl.rug.digitallab.gradle.plugin.common.CommonKotlinPlugin
import nl.rug.digitallab.gradle.plugin.common.CommonPlugin
import nl.rug.digitallab.gradle.plugin.common.publish.PublishablePlugin

val projectGroupId: String by project
val javaVersion: String by project

plugins {
    id("com.gradle.plugin-publish") // Automatically applies the `maven-publish` plugin, and `java-gradle-plugin` plugin.
    kotlin("jvm")
}

allprojects {
    apply(plugin = "com.gradle.plugin-publish")
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply<CommonKotlinPlugin>()
    apply<PublishablePlugin>() // PublishablePlugin should always be applied after com.gradle.plugin-publish

    group = projectGroupId

    repositories {
        mavenCentral()
        gradlePluginPortal()
    }

    tasks.test {
        useJUnitPlatform()
    }

    tasks.processResources {
        from(rootDir) {
            // Add the gradle.properties file to the resources, and rename it to version.properties
            // This is done so that the versions can be read from the resources at runtime, instead
            // of hardcoding them. The VersionPropertiesUtils class in the common subproject can
            // be used to read the properties from the resources.
            include("gradle.properties")
            rename("gradle.properties", "version.properties")
        }
    }
}
