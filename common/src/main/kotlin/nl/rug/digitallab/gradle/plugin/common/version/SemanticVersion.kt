package nl.rug.digitallab.gradle.plugin.common.version

/**
 * A semantic version following the semantic versioning 2.0 specification.
 *
 * @param major The major version.
 * @param minor The minor version.
 * @param patch The patch version.
 * @param preRelease The pre-release string.
 * @param buildMetaData The build metadata.
 * @throws IllegalArgumentException if the version is not a valid semantic version.
 * @see <a href="https://semver.org/">Semantic Versioning 2.0</a>
 */
data class SemanticVersion(
    val major: Long,
    val minor: Long,
    val patch: Long,
    val preRelease: String? = null,
    val buildMetaData: String? = null
): Comparable<SemanticVersion> {
    companion object {
        // Semantic versioning 2.0 regex from https://semver.org/
        private val FULL_REGEX =
            """^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$""".toRegex()

        /**
         * Parses a string into a [SemanticVersion].
         *
         * @param string The string to parse.
         * @return The parsed [SemanticVersion].
         *
         * @throws IllegalArgumentException if the string is not a valid semantic version.
         */
        fun parse(string: String): SemanticVersion {
            val matchResult = match(string)

            val (major, minor, patch, preRelease, buildMetaData) = matchResult.destructured

            return SemanticVersion(
                major.toLong(),
                minor.toLong(),
                patch.toLong(),
                preRelease.ifEmpty { null },
                buildMetaData.ifEmpty { null }
            )
        }

        /**
         * Checks if a string is a valid semantic version.
         *
         * @param string The string to check.
         * @return `true` if the string is a valid semantic version, `false` otherwise.
         */
        fun isValid(string: String): Boolean {
            return try {
                match(string)
                true
            } catch (e: IllegalArgumentException) {
                false
            }
        }

        /**
         * Match a string as a semantic version and returns the [MatchResult].
         *
         * @param string The string to validate.
         *
         * @return The [MatchResult] of the validation if the string is a valid semantic version.
         *
         * @throws IllegalArgumentException if the string is not a valid semantic version.
         */
        private fun match(string: String): MatchResult {
            val matchResult = FULL_REGEX.matchEntire(string)
            requireNotNull(matchResult)
            return matchResult
        }
    }

    init {
        require(major >= 0) { "Major version must be non-negative, is: $major" }
        require(minor >= 0) { "Minor version must be non-negative, is: $minor" }
        require(patch >= 0) { "Patch version must be non-negative, is: $patch" }
        require(isValid(toString())) { "Invalid semantic version, is: ${toString()}" }
    }

    /**
     * Returns the string representation of the semantic version.
     *
     * @return The string representation of the semantic version.
     */
    override fun toString(): String {
        val version = "$major.$minor.$patch"
        val preRelease = preRelease?.let { "-$it" } ?: ""
        val buildMetaData = buildMetaData?.let { "+$it" } ?: ""
        return "$version$preRelease$buildMetaData"
    }

    /**
     * Compares this object with the specified object for order. Returns zero if this object is equal to the specified
     * other object, a negative number if it's less than other, or a positive number if it's greater than other.
     *
     * @param other The semantic version to compare to.
     *
     * @return A negative number if this object is less than the other object, zero if it's equal, or a positive number if
     * it's greater.
     */
    override fun compareTo(other: SemanticVersion): Int =
        listOf(
            major.compareTo(other.major),
            minor.compareTo(other.minor),
            patch.compareTo(other.patch),
            preRelease.compareTo(other.preRelease),
            buildMetaData.compareTo(other.buildMetaData)
        ).reduce { acc, c -> if (acc == 0) c else acc }


    /**
     * Compares two nullable strings. If both strings are null, 0 is returned. If only one of the strings is null, the
     * non-null string is considered greater. If both strings are non-null, a lexicographical comparison is made.
     *
     * @param other The string to compare to.
     *
     * @return A negative number if this string is less than other, zero if they are equal, or a positive number if
     * this string is greater.
     */
    private operator fun String?.compareTo(other: String?): Int {
        if(this == null && other == null) return 0
        if(this == null) return 1
        if(other == null) return -1
        return this.compareTo(other)
    }

    /*
     * Syntactic sugar for updating the semantic version.
     */

    fun incrementMajor(): SemanticVersion = copy(major = major + 1, minor = 0, patch = 0)
    fun incrementMinor(): SemanticVersion = copy(minor = minor + 1, patch = 0)
    fun incrementPatch(): SemanticVersion = copy(patch = patch + 1)
    fun updatePreRelease(preRelease: String?): SemanticVersion = copy(preRelease = preRelease)
    fun updateBuildMetaData(buildMetaData: String?): SemanticVersion = copy(buildMetaData = buildMetaData)
}
