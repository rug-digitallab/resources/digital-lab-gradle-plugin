package nl.rug.digitallab.gradle.util

import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import org.gradle.api.Project
import java.net.URL
import java.nio.file.Path
import java.util.Properties
import kotlin.io.path.Path

/**
 * Utility class for working with the version.properties file in the resources folder.
 */
object VersionPropertiesUtils {
    /**
     * Reads a property from the version.properties file.
     *
     * @param key The key of the property to read.
     *
     * @return The value of the property.
     */
    fun Project.readVersionProperty(key: String): String =
        project.properties[key] as String?
            ?: readProperty(key)

    /**
     * Reads a property from the version.properties file.
     *
     * @param key The key of the property to read.
     * @return The value of the property.
     */
    private fun readProperty(key: String): String =
        parseProperties(getResource("version.properties")).getProperty(key)

    /**
     * Parses a [Properties] file.
     *
     * @param file The [Path] of the file to parse.
     * @return The [Properties] object.
     */
    private fun parseProperties(file: URL): Properties =
        file.openStream().use {
            Properties().apply { load(it) }
        }
}
