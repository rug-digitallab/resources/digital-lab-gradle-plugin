package nl.rug.digitallab.gradle.plugin.common

import nl.rug.digitallab.gradle.plugin.common.git.GitPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.sonarqube.gradle.SonarExtension
import org.sonarqube.gradle.SonarQubePlugin

/**
 * The [CommonRootPlugin] is a plugin that applies common plugins and configurations to the root project. The
 * [CommonPlugin] ensures the [CommonRootPlugin] is applied to the root project only once.
 */
class CommonRootPlugin: Plugin<Project> {
    override fun apply(project: Project) = applyToRoot(project.rootProject)

    private fun applyToRoot(rootProject: Project) {
        with(rootProject.pluginManager) {
            apply(GitPlugin::class.java)
            apply(SonarQubePlugin::class.java)
        }

        with(rootProject.extensions) {
            configure(SonarExtension::class.java) { extension ->
                extension.properties {
                    // Assumes that the name of the project is the same as the name of the repository
                    it.property("sonar.projectKey", "rug-digitallab_${rootProject.name}")
                    it.property("sonar.organization", "rug-digitallab")
                    it.property("sonar.coverage.jacoco.xmlReportPaths", "**/build/**/jacoco*.xml")
                }
            }
        }

        with(rootProject.tasks) {
            register("listPlugins", ListPluginsTask::class.java)
        }
    }
}

