package nl.rug.digitallab.gradle.plugin.common.publish

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.credentials.HttpHeaderCredentials
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin
import org.gradle.authentication.http.HttpHeaderAuthentication
import org.gradle.jvm.tasks.Jar
import org.jetbrains.dokka.gradle.DokkaPlugin
import org.jetbrains.dokka.gradle.DokkaTask

/**
 * The PublishablePlugin is a plugin that is used to configure a project to be publishable to a Maven repository. It is
 * not a plugin that can be applied manually by the user. Instead, it is applied by other plugins that want to make a
 * project publishable.
 */
class PublishablePlugin: Plugin<Project> {
    override fun apply(project: Project) {
        // Check if the isGradlePlugin property is set, otherwise try to infer if the project is a Gradle plugin project.
        val isGradlePlugin = (project.properties["isGradlePlugin"] as String?)
            ?.toBooleanStrictOrNull() ?: hasGradlePluginPublish(project)

        with(project.pluginManager) {
            apply(DokkaPlugin::class.java)
            apply(MavenPublishPlugin::class.java)
        }

        with(project.tasks) {
            // Setup the Dokka tasks, Dokka HTML is generated and added as javadoc.
            register("dokkaHtmlJar", Jar::class.java) {
                it.group = "documentation"
                it.dependsOn(project.tasks.getByName("dokkaHtml"))
                it.from((project.tasks.getByName("dokkaHtml") as DokkaTask).outputDirectory)
                it.archiveClassifier.set("html-docs")
            }

            register("dokkaJavadocJar", Jar::class.java) {
                it.group = "documentation"
                it.dependsOn(project.tasks.getByName("dokkaJavadoc"))
                it.from((project.tasks.getByName("dokkaJavadoc") as DokkaTask).outputDirectory)
                it.archiveClassifier.set("javadoc")
            }
        }

        // Setup Maven repositories for publishing
        with(project.extensions) {
            configure(PublishingExtension::class.java) { publishingExtension ->
                with(publishingExtension) {
                    repositories { repositories ->
                        repositories.maven { mavenRepository ->
                            with(mavenRepository) {
                                url = project.uri(project.findProperty("mavenRepositoryUrl").toString())
                                name = project.findProperty("mavenRepositoryName").toString()

                                credentials(HttpHeaderCredentials::class.java) {
                                    it.name = project.findProperty("mavenRepositoryHeaderName").toString()
                                    it.value = project.findProperty("mavenRepositoryHeaderValue").toString()
                                }

                                authentication.apply {
                                    create("header", HttpHeaderAuthentication::class.java)
                                }
                            }
                        }
                    }
                }
            }
        }

        // When the Gradle Publishing Plugin is present, no additional publications should be set up, otherwise each
        // plugin will be published twice.
        if(!isGradlePlugin)
            setupPublications(project)
    }

    /**
     * Check if the project has the Gradle Plugin Publish plugin applied.
     *
     * @param project The project to check.
     *
     * @return True if the project has the Gradle Plugin Publish plugin applied, false otherwise.
     */
    private fun hasGradlePluginPublish(project: Project): Boolean {
        with(project.pluginManager) {
            return this.hasPlugin("com.gradle.plugin-publish")
        }
    }

    /**
     * Set up the Maven publications for the project.
     *
     * @param project The project to set up the Maven publications for.
     */
    private fun setupPublications(project: Project) {
        with(project.extensions) {
            configure(PublishingExtension::class.java) { publishingExtension ->
                with(publishingExtension) {
                    publications.create(project.name, MavenPublication::class.java).apply {
                        from(project.components.getByName("java"))
                        artifact(project.tasks.getByName("dokkaHtmlJar"))
                    }
                }
            }
        }
    }
}
