package nl.rug.digitallab.gradle.plugin.common

import nl.rug.digitallab.gradle.util.VersionPropertiesUtils.readVersionProperty
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.noarg.gradle.KotlinJpaSubplugin
import org.jetbrains.kotlin.noarg.gradle.NoArgExtension

/**
 * The [CommonKotlinPlugin] is a plugin tha is used for shared configuration between all Digital Lab
 * Gradle plugins for Kotlin. Whatever is in here will be applied to all Kotlin plugins.
 */
class CommonKotlinPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val javaVersion = project.readVersionProperty("javaVersion")
        val kotestVersion = project.readVersionProperty("kotestVersion")
        val commonKotlinVersion = project.readVersionProperty("commonKotlinVersion")
        val includeCommonLibraries = (project.properties["includeCommonLibraries"] as String?)
            ?.toBooleanStrictOrNull() ?: true

        with(project.pluginManager) {
            // Apply the plugins used by all Digital Lab projects
            apply(KotlinPluginWrapper::class.java)
            apply(KotlinJpaSubplugin::class.java) // Add noarg plugin with JPA preset.
            apply(CommonPlugin::class.java) // Add the base common plugin
        }

        if (includeCommonLibraries) {
            with(project.dependencies) {
                // Add the common kotlin dependencies to all our projects
                add("implementation", "nl.rug.digitallab.common.kotlin:quantities:$commonKotlinVersion")
                add("implementation", "nl.rug.digitallab.common.kotlin:helpers:$commonKotlinVersion")
            }
        }

        with(project.dependencies) {
            // Add Kotest Assertions for all Kotlin projects
            add("testImplementation", "io.kotest:kotest-assertions-core:$kotestVersion")
        }

        with(project.tasks) {
            // Set up the Kotlin task
            withType(KotlinCompile::class.java).configureEach {
                it.compilerOptions {
                    jvmTarget.set(JvmTarget.fromTarget(javaVersion))
                    javaParameters.set(true)
                    freeCompilerArgs.add("-Xcontext-receivers") // Enable Kotlin Context Receivers.
                }
            }
        }

        with(project.extensions) {
            // Set up the NoArg extension. This is needed because CDI requires a no-arg constructor for its proxies. And
            // Kotlin data classes do not have a no-arg constructor by default.
            configure(NoArgExtension::class.java) {
                it.annotation("nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor")
            }
        }
    }
}
