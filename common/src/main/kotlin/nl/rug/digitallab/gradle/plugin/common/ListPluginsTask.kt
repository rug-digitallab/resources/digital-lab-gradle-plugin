package nl.rug.digitallab.gradle.plugin.common

import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction

/**
 * The [ListPluginsTask] is a task that lists all plugins applied to the project and its subprojects.
 */
abstract class ListPluginsTask: DefaultTask() {
    init {
        group = "Help"
        description = "List all plugins applied to the project and its subprojects."
    }

    @TaskAction
    fun listPlugins() {
        listPlugins(project).forEach { (projectName, plugins) ->
            plugins.forEach { plugin -> logger.lifecycle("$projectName:$plugin") }
        }
    }

    /**
     * Recursively list all plugins applied to the project and its subprojects.
     *
     * @param project The project to list the plugins for.
     * @param pluginMap The map to store the plugins in.
     *
     * @return A map of project names to a list of plugin class names.
     */
    private fun listPlugins(project: Project, pluginMap: MutableMap<String, List<String>> = mutableMapOf()): Map<String, List<String>> {
        pluginMap[project.name] = project.plugins.map { it::class.qualifiedName!! }
        project.subprojects.forEach { listPlugins(it, pluginMap) }
        return pluginMap
    }
}
