package nl.rug.digitallab.gradle.plugin.common.version

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * The PrintVersionTask is a task that prints the project version to the console.
 */
abstract class PrintVersionTask: DefaultTask() {
    init {
        group = "digital lab"
        description = "Prints the project version."
    }

    @TaskAction
    fun print() {
        logger.lifecycle("Project version: ${project.version}")
    }
}
