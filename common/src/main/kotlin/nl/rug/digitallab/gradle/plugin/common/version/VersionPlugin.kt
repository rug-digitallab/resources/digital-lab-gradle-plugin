package nl.rug.digitallab.gradle.plugin.common.version

import org.gradle.api.Plugin
import org.gradle.api.Project
import java.io.File

/**
 * The VersionPlugin is a plugin that reads the project version from a VERSION file in the project root, and sets
 * the project version to the contents of that file. It also adds a task to print the version to the console. Finally,
 * it ensures that the version in the VERSION file is a valid semantic version.
 */
class VersionPlugin: Plugin<Project> {
    override fun apply(project: Project): Unit = with(project) {
        val versionFile = findVersionFile(project)
            ?: throw IllegalStateException("No VERSION file found in the root of the project!")

        // If a version file is available, read it
        val versionString = versionFile.readText().trim()

        // Ensure that the version is a valid semantic version
        require(SemanticVersion.isValid(versionString)) {
            "Version '$versionString' specified for project '$name' is not a valid semantic version"
        }

        // Set the version of the project to the version read from the VERSION file.
        // This does not prevent the version from being overwritten later; during the plugin apply phase the version
        // is not yet set, and therefore we cannot check if the manually set version is a semantic version.
        version = versionString

        with(tasks) {
            register("printVersion", PrintVersionTask::class.java)
        }
    }

    /**
     * Finds the version file in the root project.
     *
     * @param project The project to find the version file in.
     * @return The version file. If it does not exist, null is returned.
     */
    private fun findVersionFile(project: Project): File? {
        val rootVersionFile = project.rootProject.file("VERSION")
        if(rootVersionFile.exists()) {
            return rootVersionFile
        }

        return null
    }
}
