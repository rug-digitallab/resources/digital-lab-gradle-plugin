package nl.rug.digitallab.gradle.plugin.common

import nebula.plugin.dependencylock.DependencyLockPlugin
import nl.rug.digitallab.gradle.plugin.common.version.VersionPlugin
import nl.rug.digitallab.gradle.util.VersionPropertiesUtils.readVersionProperty
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.testing.Test
import org.gradle.testing.jacoco.plugins.JacocoPlugin
import org.gradle.testing.jacoco.tasks.JacocoReport
import java.net.URI

/**
 * The [CommonPlugin] is a plugin that is used for shared configuration between all Digital Lab Gradle plugins.
 * Whatever is in here will be applied to all plugins.
 */
class CommonPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        val javaVersion = project.readVersionProperty("javaVersion")
        val projectGroupId = (project.properties["projectGroupId"] as String?)
            ?: throw IllegalStateException("No 'projectGroupId' property found in the project properties.")

        project.group = projectGroupId

        with(project.pluginManager) {
            // Apply the plugins used by all Digital Lab projects
            apply(VersionPlugin::class.java)

            // The Java plugins still needs to be applied, even for Kotlin projects
            apply(JavaPlugin::class.java)

            // Apply the Jacoco plugin for code coverage
            apply(JacocoPlugin::class.java)

            // Apply the dependency lock plugin, which is used to pass dependency information to Dependency Scanning.
            apply(DependencyLockPlugin::class.java)
        }

        with(project.repositories) {
            // Set up all the typical repositories to pull dependencies from
            gradlePluginPortal()
            mavenCentral()
            mavenLocal()

            // Add the Digital Lab group-level Maven repository
            maven {
                it.name = "Digital Lab"
                it.url = URI.create("https://gitlab.com/api/v4/groups/65954571/-/packages/maven")
            }
        }

        with(project.extensions) {
            // Set up the Java extension
            configure(JavaPluginExtension::class.java) {
                it.sourceCompatibility = JavaVersion.toVersion(javaVersion)
                it.targetCompatibility = JavaVersion.toVersion(javaVersion)
                it.withSourcesJar() // Automatically adds the sources JAR as an artifact to the publications.
            }
        }

        with(project.rootProject) {
            plugins.find { it::class.qualifiedName == CommonRootPlugin::class.qualifiedName } ?: pluginManager.apply(CommonRootPlugin::class.java)
        }

        with(project.tasks) {
            // Set up the test task such that the jacoco report always runs after the tests to generate a code coverage report
            withType(Test::class.java).configureEach {
                it.useJUnitPlatform()
                it.finalizedBy("jacocoTestReport")
            }

            // Set up the jacocoTestReport task to generate the code coverage report in XML format.
            withType(JacocoReport::class.java).configureEach { jacocoReport ->
                jacocoReport.dependsOn("test")
                jacocoReport.reports {
                    it.xml.required.set(true)
                }
            }
        }
    }
}
