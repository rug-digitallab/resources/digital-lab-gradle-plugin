package nl.rug.digitallab.gradle.plugin.common

import nl.rug.digitallab.gradle.plugin.common.version.VersionPlugin
import nl.rug.digitallab.gradle.util.VersionPropertiesUtils.readVersionProperty
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.noarg.gradle.KotlinJpaSubplugin
import org.jetbrains.kotlin.noarg.gradle.NoArgExtension
import java.net.URI

/**
 * The [CommonJavaPlugin] is a plugin tha is used for shared configuration between all Digital Lab
 * Gradle plugins for Java. Whatever is in here will be applied to all Java plugins.
 */
class CommonJavaPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        // Currently the Common Java plugin acts as an alias for the CommonPlugin,
        // however it exists for further configuration to be easily applied to just the
        // java plugins.
        with(project.pluginManager) {
            apply(CommonPlugin::class.java)
        }
    }
}
