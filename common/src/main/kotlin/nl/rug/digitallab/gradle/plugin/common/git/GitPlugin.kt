package nl.rug.digitallab.gradle.plugin.common.git

import nl.rug.digitallab.gradle.plugin.common.version.VersionPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * The GitPlugin adds the startRelease task to the project.
 */
class GitPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        with(project.pluginManager) {
            apply(VersionPlugin::class.java)
        }

        with(project.tasks) {
            register("startRelease", StartReleaseTask::class.java)
        }
    }
}
