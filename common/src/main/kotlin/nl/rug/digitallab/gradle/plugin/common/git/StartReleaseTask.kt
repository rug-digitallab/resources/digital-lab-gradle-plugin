package nl.rug.digitallab.gradle.plugin.common.git

import nl.rug.digitallab.gradle.plugin.common.version.SemanticVersion
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.RepositoryBuilder
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * The StartReleaseTask is a task that starts a release. There are 3 main steps:
 * 1. Calculate the target release and develop versions.
 * 2. Perform sanity checks before starting the release.
 * 3. Set the release version in the develop branch, create the release branch, and set the develop version in the develop
 *   branch.
 */
abstract class StartReleaseTask: DefaultTask() {
    private val developBranch = "develop"
    private val releaseBranchPrefix = "release"

    init {
        group = "digital lab"
        description = "Starts the release of a new version by bumping versions and creating a release branch."
    }

    @TaskAction
    fun startRelease() = with(project.rootProject) {
        // Calculate the target release and develop versions
        val (targetReleaseVersion, targetDevelopVersion) = calculateTargetVersions()
        val releaseBranch = "$releaseBranchPrefix/$targetReleaseVersion"

        openGitRepository { git ->
            // Perform sanity checks before starting the release
            git.doReleaseChecks(releaseBranch)

            // Set the release version in the develop branch
            git.setVersionFile(targetReleaseVersion, developBranch)

            // Create the release branch
            git.branchCreate()
                .setName(releaseBranch)
                .setStartPoint(developBranch)
                .call()

            logger.lifecycle("Branch '$releaseBranch' created")

            // Set the develop version in the develop branch
            git.setVersionFile(targetDevelopVersion, developBranch)

            // Check out the release branch
            git.checkout()
                .setName(releaseBranch)
                .call()

            logger.lifecycle("Checked out branch '$releaseBranch'")

            logger.lifecycle("You must now push both the '$developBranch' branch and the '$releaseBranch' branch to the remote repository:")
            logger.lifecycle("git push --set-upstream origin $releaseBranch")
            logger.lifecycle("git checkout $developBranch")
            logger.lifecycle("git push")
        }
    }

    /**
     * Calculate the target release and develop versions. The target release version is the version that the release
     * branch will be created from. The target develop version is the version that the develop branch will be updated to
     * after the release branch is created.
     *
     * @return A pair containing the target release version and the target develop version.
     *
     * @throws IllegalArgumentException If the version properties are not set correctly.
     */
    private fun calculateTargetVersions(): Pair<SemanticVersion, SemanticVersion> {
        // Collect the version information
        val currentVersion = SemanticVersion.parse(project.version.toString())
        val targetReleaseVersion = currentVersion.updatePreRelease(null)
        val targetDevelopVersion = getSemanticVersionFromProperty("targetDevelopVersion")
            ?: targetReleaseVersion.incrementPatch().updatePreRelease("SNAPSHOT")

        // Ensure that the develop version is a snapshot version
        require(targetDevelopVersion.preRelease == "SNAPSHOT") {
            "The target develop version must be a SNAPSHOT version"
        }

        require(targetReleaseVersion > currentVersion) {
            "The target release version must be greater than the current version"
        }

        require(targetDevelopVersion > targetReleaseVersion) {
            "The target develop version must be greater than the target release version"
        }

        // Log the version information
        logger.lifecycle("Current version is '$currentVersion'")
        logger.lifecycle("Target release version is '$targetReleaseVersion'")
        logger.lifecycle("Target develop version is '$targetDevelopVersion'")

        return targetReleaseVersion to targetDevelopVersion
    }

    /**
     * Gets a semantic version from a Gradle property. If the property is not set, `null` is returned.
     *
     * @param propertyName The name of the Gradle property.
     *
     * @return The semantic version or `null` if the property is not set.
     */
    private fun getSemanticVersionFromProperty(propertyName: String): SemanticVersion? =
        if(project.hasProperty(propertyName)) {
            logger.lifecycle("Using property '$propertyName' as semantic version")
            SemanticVersion.parse(project.property(propertyName).toString())
        } else {
            null
        }

    /**
     * Opens the git repository and executes the given block of code. The repository is closed after the block has been
     * executed.
     *
     * @param block The block of code to execute.
     */
    private fun openGitRepository(block: (Git) -> Unit) {
        val repositoryBuilder = RepositoryBuilder().findGitDir(project.rootDir)
        requireNotNull(repositoryBuilder.gitDir) {
            "'${project.rootDir}' does not belong to a git repository!"
        }

        repositoryBuilder.build().use { repository ->
            block(Git(repository))
        }
    }

    /**
     * Perform sanity checks before starting the release.
     *
     * @param releaseBranch The name of the release branch.
     *
     * @throws IllegalStateException If any of the checks fail.
     */
    private fun Git.doReleaseChecks(releaseBranch: String) {
        // Ensure there are no uncommitted changes
        check(!(this.status().call().hasUncommittedChanges())) {
            "There are uncommitted changes in the repository!"
        }

        // Ensure that the develop branch exists
        val branchList = this.branchList().call()
        check(branchList.any { it.name == "refs/heads/$developBranch" }) {
            "The develop branch '$developBranch' does not exist!"
        }

        // Ensure that the release branch does not exist yet
        check(branchList.none { it.name == "refs/heads/$releaseBranch" }) {
            "The release branch '$releaseBranch' already exists!"
        }

        // Ensure that the release task is run from the develop branch
        val currentBranch = this.repository.branch
        check(currentBranch == developBranch) {
            "The release task must be run from the '$developBranch' branch, but is currently run from branch '$currentBranch'"
        }

        // Log presence of SNAPSHOT dependencies
        project.configurations.flatMap { it.dependencies }
            .filter { it.version?.endsWith("-SNAPSHOT") ?: false }
            .map { it.group + ":" + it.name + ":" + it.version }
            .forEach { logger.warn("SNAPSHOT dependency found: $it") }
    }

    /**
     * Set the contents of a VERSION file in a Git repository for a given branch. The branch is checked out, the
     * version file is updated, and the changes are committed.
     *
     * @param version The version to set.
     * @param branch The branch to set the version in.
     */
    private fun Git.setVersionFile(
        version: SemanticVersion,
        branch: String,
    ) {
        // Checkout the branch
        this.checkout()
            .setName(branch)
            .call()

        // Change version file
        val versionFile = project.file("VERSION")
        versionFile.writeText(version.toString())

        // Commit the version change
        this.add()
            .addFilepattern("VERSION")
            .call()

        this.commit()
            .setMessage("Version set to '$version'")
            .call()

        logger.lifecycle("VERSION file set to '$version' in branch '$branch'")
    }
}
