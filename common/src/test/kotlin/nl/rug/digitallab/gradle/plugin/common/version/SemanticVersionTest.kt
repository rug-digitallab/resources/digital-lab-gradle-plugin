package nl.rug.digitallab.gradle.plugin.common.version

import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.equals.shouldBeEqual
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import kotlin.random.Random

class SemanticVersionTest {
    @TestFactory
    fun `Parsing a valid semantic version should succeed`(): List<DynamicTest> {
        return validVersions.map {
            DynamicTest.dynamicTest("Parsing `$it` should succeed") {
                shouldNotThrowAny { SemanticVersion.parse(it) }
            }
        }
    }

    @TestFactory
    fun `Parsing an invalid semantic version should fail`(): List<DynamicTest> {
        return invalidVersions.map {
            DynamicTest.dynamicTest("Parsing `$it` should fail") {
                shouldThrow<IllegalArgumentException> { SemanticVersion.parse(it) }
            }
        }
    }

    @TestFactory
    fun `Parsing a valid semantic version and then formatting it should return the original version`(): List<DynamicTest> {
        return validVersions.map {
            DynamicTest.dynamicTest("Parsing `$it` and then formatting it should return the original version") {
                SemanticVersion.parse(it)
                    .toString()
                    .shouldBeEqual(it)
            }
        }
    }

    @Test
    fun `Versions should be sorted in the correct order`() {
        val sortedVersionList = listOf(
            "1.0.0-SNAPSHOT",
            "1.0.0",
            "1.0.1",
            "1.1.0",
            "1.1.1",
            "1.2.0",
            "2.0.0-SNAPSHOT",
            "2.0.0",
            "2.1.0",
            "2.1.1-SNAPSHOT+meta",
            "2.1.1-SNAPSHOT",
            "2.1.1",
        ).map { SemanticVersion.parse(it) }

        val random = Random(seed = 149269201)
        val shuffledVersionList = sortedVersionList
            .shuffled(random) // Shuffle the list

        shuffledVersionList.sorted().shouldContainExactly(sortedVersionList)
    }

    @Test
    fun `Incrementing the major version should increment the major version`() {
        val version = SemanticVersion(1, 2, 3)
        val incremented = version.incrementMajor()

        incremented.shouldBeEqual(version.copy(major = version.major + 1, minor = 0, patch = 0))
    }

    @Test
    fun `Incrementing the minor version should increment the minor version`() {
        val version = SemanticVersion(1, 2, 3)
        val incremented = version.incrementMinor()

        incremented.shouldBeEqual(version.copy(minor = version.minor + 1, patch = 0))
    }

    @Test
    fun `Incrementing the patch version should increment the patch version`() {
        val version = SemanticVersion(1, 2, 3)
        val incremented = version.incrementPatch()

        incremented.shouldBeEqual(version.copy(patch = version.patch + 1))
    }

    @Test
    fun `Updating the pre-release should update the pre-release`() {
        val version = SemanticVersion(1, 2, 3, "alpha")
        val updated = version.updatePreRelease("beta")

        updated.shouldBeEqual(version.copy(preRelease = "beta"))
    }

    @Test
    fun `Updating the pre-release with an invalid pre-release should fail`() {
        val version = SemanticVersion(1, 2, 3, "alpha")
        shouldThrow<IllegalArgumentException> { version.updatePreRelease("bet@") }
    }

    @Test
    fun `Updating the build meta-data should update the build meta-data`() {
        val version = SemanticVersion(1, 2, 3, buildMetaData = "build")
        val updated = version.updateBuildMetaData("newBuild")

        updated.shouldBeEqual(version.copy(buildMetaData = "newBuild"))
    }

    @Test
    fun `Updating the build meta-data with an invalid build meta-data should fail`() {
        val version = SemanticVersion(1, 2, 3, buildMetaData = "build")
        shouldThrow<IllegalArgumentException> { version.updateBuildMetaData("m3t@") }
    }

    @Test
    fun `Copying a Semantic Version should still validate the copy`() {
        val version = SemanticVersion(1, 2, 3, "alpha", "build")
        shouldThrow<IllegalArgumentException> { version.copy(major = -1) }
    }

    /*
     * Valid/invalid examples extracted from https://semver.org/
     */

    private val invalidVersions = listOf(
        "1",
        "1.2",
        "1.2.3-0123",
        "1.2.3-0123.0123",
        "1.1.2+.123",
        "+invalid",
        "-invalid",
        "-invalid+invalid",
        "-invalid.01",
        "alpha",
        "alpha.beta",
        "alpha.beta.1",
        "alpha.1",
        "alpha+beta",
        "alpha_beta",
        "alpha.",
        "alpha..",
        "        beta",
        "1.0.0-alpha_beta",
        "-alpha.",
        "1.0.0-alpha..",
        "        1.0.0-alpha..1",
        "1.0.0-alpha...1",
        "1.0.0-alpha....1",
        "1.0.0-alpha.....1",
        "1.0.0-alpha......1",
        "1.0.0-alpha.......1",
        "01.1.1",
        "1.01.1",
        "1.1.01",
        "1.2",
        "1.2.3.DEV",
        "1.2-SNAPSHOT",
        "1.2.31.2.3----RC-SNAPSHOT.12.09.1--..12+788",
        "1.2-RC-SNAPSHOT",
        "-1.0.3-gamma+b7718",
        "+justmeta",
        "9.8.7+meta+meta",
        "9.8.7-whatever+meta+meta",
        "99999999999999999999999.999999999999999999.99999999999999999----RC-SNAPSHOT.12.09.1--------------------------------..12",
    )

    private val validVersions = listOf(
        "0.0.4",
        "1.2.3",
        "10.20.30",
        "1.1.2-prerelease+meta",
        "1.1.2+meta",
        "1.1.2+meta-valid",
        "1.0.0-alpha",
        "1.0.0-beta",
        "1.0.0-alpha.beta",
        "1.0.0-alpha.beta.1",
        "1.0.0-alpha.1",
        "1.0.0-alpha0.valid",
        "1.0.0-alpha.0valid",
        "1.0.0-alpha-a.b-c-somethinglong+build.1-aef.1-its-okay",
        "1.0.0-rc.1+build.1",
        "2.0.0-rc.1+build.123",
        "1.2.3-beta",
        "10.2.3-DEV-SNAPSHOT",
        "1.2.3-SNAPSHOT-123",
        "1.0.0",
        "2.0.0",
        "1.1.7",
        "2.0.0+build.1848",
        "2.0.1-alpha.1227",
        "1.0.0-alpha+beta",
        "1.2.3----RC-SNAPSHOT.12.9.1--.12+788",
        "1.2.3----R-S.12.9.1--.12+meta",
        "1.2.3----RC-SNAPSHOT.12.9.1--.12",
        "1.0.0+0.build.1-rc.10000aaa-kk-0.1",
        "999999999999999999.999999999999999999.99999999999999999",
        "1.0.0-0A.is.legal",
    )
}
