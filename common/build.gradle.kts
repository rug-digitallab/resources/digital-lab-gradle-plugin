// Anything added here should also be added to buildSrc/build.gradle.kts
dependencies {
    val dokkaVersion: String by project
    val jgitVersion: String by project
    val junitVersion: String by project
    val kotlinVersion: String by project
    val sonarqubeVersion: String by project
    val commonKotlinVersion: String by project
    val kotestVersion: String by project
    val dependencyLockVersion: String by project

    implementation("nl.rug.digitallab.common.kotlin:helpers:$commonKotlinVersion")

    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-noarg:$kotlinVersion")
    implementation("org.jetbrains.dokka:dokka-gradle-plugin:$dokkaVersion")
    implementation("org.eclipse.jgit:org.eclipse.jgit:$jgitVersion")
    implementation("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:$sonarqubeVersion")
    implementation("com.netflix.nebula.dependency-lock:com.netflix.nebula.dependency-lock.gradle.plugin:$dependencyLockVersion")

    testImplementation(platform("org.junit:junit-bom:$junitVersion"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.junit.platform:junit-platform-launcher")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
}
