/**
 * This init.gradle.kts script adds the Maven Local repository to the plugin management repositories.
 * It is only used during integration tests in the CI pipeline.
 */
class MavenLocalPlugin: Plugin<Gradle> {
    override fun apply(gradle: Gradle) {
        gradle.settingsEvaluated {
            pluginManagement {
                repositories {
                    mavenLocal()
                }
            }
        }
    }
}

apply<MavenLocalPlugin>()

