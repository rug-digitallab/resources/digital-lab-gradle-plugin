rootProject.name = "digital-lab-gradle-plugin"

apply("shared-settings.gradle.kts")

include("common")
include("plugins")
include("test-suite")
