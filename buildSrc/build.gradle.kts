plugins {
    kotlin("jvm")
}

// The dependencies should always be the same as the dependencies declared in common/build.gradle.kts
// We cannot apply a shared `common-build.gradle.kts` to both buildSrc and common due to type safe model accessors
// https://docs.gradle.org/current/userguide/kotlin_dsl.html#type-safe-accessors
dependencies {
    val dokkaVersion: String by project
    val jgitVersion: String by project
    val junitVersion: String by project
    val kotlinVersion: String by project
    val sonarqubeVersion: String by project
    val commonKotlinVersion: String by project
    val kotestVersion: String by project
    val dependencyLockVersion: String by project

    implementation("nl.rug.digitallab.common.kotlin:helpers:$commonKotlinVersion")

    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-noarg:$kotlinVersion")
    implementation("org.jetbrains.dokka:dokka-gradle-plugin:$dokkaVersion")
    implementation("org.eclipse.jgit:org.eclipse.jgit:$jgitVersion")
    implementation("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:$sonarqubeVersion")
    implementation("com.netflix.nebula.dependency-lock:com.netflix.nebula.dependency-lock.gradle.plugin:$dependencyLockVersion")

    testImplementation(platform("org.junit:junit-bom:$junitVersion"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.junit.platform:junit-platform-launcher")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
}

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Group Repository
    gradlePluginPortal()
}

kotlin {
    sourceSets["main"].kotlin.srcDir("../common/src/main/kotlin")
}
